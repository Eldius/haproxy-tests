#!/bin/bash

CURR_PATH="${PWD}"

clear

cd $CURR_PATH; \
    docker network prune -f && \
    docker-compose down && \
    docker-compose up

